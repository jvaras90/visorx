<?php
session_start();
if (isset($_SESSION["idp"]) && $_SESSION["idp"] != "") {
    $req_idp = $_SESSION["idp"];
    $base = "predictivo";
    $host = "localhost";
    $user = "root";
    $pass = "c0mun1Ctm4for3s";
    $conn = mysql_connect($host, $user, $pass);
    mysql_select_db($base, $conn);
    date_default_timezone_set('America/Lima');

    $_SESSION["autentificado_detenlinea"] = "SI";

    function sec_to_time($s) {
        $hors = floor($s / 3600);
        $mins = floor(($s - ($hors * 3600)) / 60);
        $segs = $s - ($hors * 3600) - ($mins * 60);
        if ($hors < 10) {
            $hors = "0" . $hors;
        }if ($mins < 10) {
            $mins = "0" . $mins;
        }if ($segs < 10) {
            $segs = "0" . $segs;
        }
        return $hors . ":" . $mins . ":" . $segs;
    }

    ////////////////MANAGER1////////////////////
    //ob_implicit_flush(false);
    $username = "monitor";
    $secret = "m0n1t0r";
    $socket = fsockopen("127.0.0.1", "5038", $errornum, $errorstr);
    $agents = array();
    $curr_agent = "";
    $better_status = array('' => '',
        'AGENT_UNKNOWN' => 'DESCONECTADO',
        'AGENT_IDLE' => 'CONECTADO',
        'AGENT_ONCALL' => 'HABLANDO',
        'AGENT_LOGGEDOFF' => 'DESCONECTADO',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5');
    $agente_status = array('LOGIN' => 'LOGIN',
        'CONECTADO' => 'ONLINE',
        'BACKOFFICE' => 'BACKOFFICE',
        'LUNCH' => 'REFRIGERIO',
        'REFRIGERIO' => 'REFRIGERIO',
        'BREAK' => 'BREAK',
        'RESUME' => 'ONLINE',
        'GESTORCORREO' => 'GESTORCORREO',
        'GESTORPROVINCIA' => 'GESTORPROVINCIA',
        'ONLINE' => 'ONLINE');
    if (!$socket) {
        print "No se puede conectar al socket. Error #" . $errornum . ": " . $errorstr;
    } else {
        fputs($socket, "Action: Login\r\n");
        fputs($socket, "UserName: " . $username . "\r\n");
        fputs($socket, "Secret: " . $secret . "\r\n\r\n");
        fputs($socket, "Action: Agents\r\n\r\n");
        fputs($socket, "Action: Logoff\r\n\r\n");
        $encoladas = 0;
        while (!feof($socket)) {
            $info = fscanf($socket, "%s\t%s\r\n");
            switch ($info[0]) {
                case "Agent:":
                    $curr_agent = $info[1];
                    $agents[$curr_agent] = array();
                    $agents[$curr_agent]['Name'] = "";
                    $agents[$curr_agent]['Status'] = "";
                    $agents[$curr_agent]['LoggedInChan'] = "";
                    $agents[$curr_agent]['LoggedInTime'] = "";
                    $agents[$curr_agent]['TalkingTo'] = "";
                    break;
                case "Name:":
                    $agents[$curr_agent]['Name'] = $info[1];
                    if ($agents[$curr_agent]['Status'] != "DESCONECTADO") {
                        $idpred_sql = "select CC_AGENTE_COLAS.CC_ID_COLAS from predictivo.CC_AGENTE_COLAS,predictivo.CC_AGENTE 
						where CC_AGENTE_COLAS.CC_ID_AGENTE=CC_AGENTE.CC_ID_AGENTE and CC_AGENTE_COLAS.CC_ESTADO='01' 
						and CC_AGENTE.CC_USUARIO='" . $curr_agent . "' and CC_AGENTE_COLAS.CC_ID_COLAS='" . $req_idp . "' limit 1";
                        //echo "\n<p>".$agents[$curr_agent]['Status']."</p>\n<p>".$idpred_sql."</p>\n";
                        $idpred_query = mysql_query($idpred_sql, $conn);
                        while ($idpred_row = mysql_fetch_array($idpred_query)) {
                            $idpred = $idpred_row["CC_ID_COLAS"];
                            $agents[$curr_agent]['Predictivo'] = $idpred;
                        }
                        mysql_free_result($idpred_query);
                    }
                    break;
                case "Status:":
                    $agents[$curr_agent]['Status'] = $better_status[$info[1]];
                    break;
                case "LoggedInChan:":
                    $agents[$curr_agent]['LoggedInChan'] = $info[1];
                    if (substr(trim($info[1]), 0, 6) == "Local/") {
                        $encoladas++;
                    }
                    break;
                case "LoggedInTime:":
                    if ($info[1] != "0") {
                        //$agents[$curr_agent]['LoggedInTime'] = date("D, M d Y g:ia", $info[1]);
                        $agents[$curr_agent]['LoggedInTime'] = date("Y-m-d h:i:s", $info[1]);
                    } else {
                        $agents[$curr_agent]['LoggedInTime'] = "";
                    }
                    break;
                case "TalkingTo:":
                    $agents[$curr_agent]['TalkingTo'] = $info[1];
                    break;
                default:
                    break;
            }
        }
        fclose($socket);
    }
    ////////////////MANAGER1////////////////////
    ////////////////MANAGER2////////////////////
    //ob_implicit_flush(false);
    $username = "monitor";
    $secret = "m0n1t0r";
    $socket = fsockopen("127.0.0.1", "5038", $errornum, $errorstr);
    $chans = array();
    $curr_chan = "";
    if (!$socket) {
        print "No se puede conectar al socket. Error #" . $errornum . ": " . $errorstr;
    } else {
        fputs($socket, "Action: Login\r\n");
        fputs($socket, "UserName: " . $username . "\r\n");
        fputs($socket, "Secret: " . $secret . "\r\n\r\n");
        fputs($socket, "Action: Status\r\n\r\n");
        fputs($socket, "Action: Logoff\r\n\r\n");
        $encoladas = 0;
        $i = 0;
        $nchans = 0;
        while (!feof($socket)) {
            $info = fscanf($socket, "%s\t%s\r\n");
            switch ($info[0]) {
                case "Channel:":
                    $i++;
                    $nchans++;
                    $curr_chan[$i] = $info[1];
                    $chans[$curr_chan[$i]] = array();
                    $chans[$curr_chan[$i]]['CallerID'] = "";
                    $chans[$curr_chan[$i]]['CallerIDNum'] = "";
                    $chans[$curr_chan[$i]]['CallerIDName'] = "";
                    $chans[$curr_chan[$i]]['Account'] = "";
                    $chans[$curr_chan[$i]]['State'] = "";
                    $chans[$curr_chan[$i]]['Context'] = "";
                    $chans[$curr_chan[$i]]['Extension'] = "";
                    $chans[$curr_chan[$i]]['Priority'] = "";
                    $chans[$curr_chan[$i]]['Seconds'] = "";
                    $chans[$curr_chan[$i]]['Link'] = "";
                    $chans[$curr_chan[$i]]['Uniqueid'] = "";
                    break;
                case "CallerID:":
                    $chans[$curr_chan[$i]]['CallerID'] = $info[1];
                    break;
                case "CallerIDNum:":
                    $chans[$curr_chan[$i]]['CallerIDNum'] = $info[1];
                    break;
                case "CallerIDName:":
                    $chans[$curr_chan[$i]]['CallerIDName'] = $info[1];
                    break;
                case "Account:":
                    $chans[$curr_chan[$i]]['Account'] = $info[1];
                    break;
                case "State:":
                    $chans[$curr_chan[$i]]['State'] = $info[1];
                    break;
                case "Context:":
                    $chans[$curr_chan[$i]]['Context'] = $info[1];
                    break;
                case "Extension:":
                    $chans[$curr_chan[$i]]['Extension'] = $info[1];
                    break;
                case "Priority:":
                    $chans[$curr_chan[$i]]['Priority'] = $info[1];
                    break;
                case "Seconds:":
                    $chans[$curr_chan[$i]]['Seconds'] = $info[1];
                    break;
                case "Link:":
                    $chans[$curr_chan[$i]]['Link'] = $info[1];
                    break;
                case "Uniqueid:":
                    $chans[$curr_chan[$i]]['Uniqueid'] = $info[1];
                    break;
                default:
                    break;
            }
        }
    }
    fclose($socket);

    ////////////////MANAGER2////////////////////	
    ?>
    <script src="js/scripts.js" type="text/javascript"></script>
    <table style="border-color:#000" align='left' border='0' cellspacing='1' cellpadding='0' bgcolor='#336699'>
        <tr>
            <td colspan="9" align="left" bgcolor="#0099FF"><font size='+1' face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF"><b>&nbsp;Detalle en Linea&nbsp;</b></font></td>
        </tr>
        <tr>
            <th width="62" rowspan="2" bgcolor='#0099FF'><font size='2' color="#FFFFFF" face='Verdana, Arial, Helvetica, sans-serif'>&nbsp;&nbsp;Anexo&nbsp;&nbsp;</font></th>
            <th width="365" rowspan="2" bgcolor='#0099FF'><font size='2' color="#FFFFFF" face='Verdana, Arial, Helvetica, sans-serif'>&nbsp;&nbsp;Agente&nbsp;&nbsp;</font></th>
            <th height='22' colspan="2" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">Estado</font></th>
            <th width="94" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">Tipo de</font></th>
            <th width="75" rowspan="2" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;Numero&nbsp;&nbsp;</font></th>
            <th width="93" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">Tiempo&nbsp;en</font></th>
            <th width="93" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">Total&nbsp;de</font></th>

            <?php
            if ($_SESSION["autentificado_detenlinea"] == "SI") {
                ?>
                <th width="90" rowspan="2" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;Logout&nbsp;&nbsp;</font></th>
                <?php
            }
            ?>
        </tr>
        <tr>

            <th width="255" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;Agente&nbsp;&nbsp;</font></th>
            <th height='25' width="150" bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;Anexo&nbsp;&nbsp;</font></th>
            <th bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;Llamada&nbsp;&nbsp;</font></th>
            <th bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;Llamada&nbsp;&nbsp;</font></th>
            <th bgcolor='#0099FF'><font size="2" color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;Llamadas&nbsp;&nbsp;</font></th>

        </tr>
        <?php
        $row = array();
        foreach ($agents as $agent => $curr) {

            $row["agente"] = $agent;
            if (substr($curr['LoggedInChan'], 0, 4) == "SIP/") {
                $row["anexo"] = substr($curr['LoggedInChan'], 4, 3);
            } elseif (substr($curr['LoggedInChan'], 3, 10) == "@atcliente") {
                $row["anexo"] = substr($curr['LoggedInChan'], 0, 3);
            } elseif (substr($curr['LoggedInChan'], 0, 5) == "Local") {
                $row["anexo"] = substr($curr['LoggedInChan'], 6, 3);
            }
            //echo  $row["agente"]."/".$curr["Predictivo"]."--";
            // BUSCAR NOMBRE DEL AGENTE EN LA BD SEGUN USUARIO LOGUEADO

            $usuario = $row["agente"];  // capturar el usuario del agente
            $NombreAgente = "";

            $row["estado_anexo"] = "LIBRE";

            $agenterows = "
			SELECT * FROM predictivo.CC_AGENTE WHERE CC_USUARIO='" . $usuario . "' limit 1 
			";
            $agenterowq = mysql_query($agenterows, $conn);
            $agenterow = "";
            while ($agenterowr = mysql_fetch_array($agenterowq)) {
                $row["nombre"] = $agenterowr["CC_NOMBRE"];
                $row["estado_anexo"] = $agenterowr["CC_STATUS"];
                $row["total_llamadas"] = $agenterowr["CC_TOTAL_LLAMADAS"];
            }
            mysql_free_result($agenterowq);

            $row["numero"] = "";
            $row["tipo_llamada"] = "-";
            if ($curr['TalkingTo'] != "n/a") {
                $row["estado_anexo"] = "HABLANDO";
                $row["numero"] = $curr['TalkingTo'];
                $row["tipo_llamada"] = "SALIENTE";
            }

            $row["estado"] = "CONECTADO"; //$curr['Status'];

            $sqlestado = "select evento as estado from predictivo.HD_ESTADOACTUAL 
			where agente='" . $row["agente"] . "' and evento<>'LOGIN' limit 1";
            $queryestado = mysql_query($sqlestado, $conn);
            while ($rowestado = mysql_fetch_array($queryestado)) {
                $row["estado"] = $rowestado["estado"];
            }
            mysql_free_result($queryestado);

            $row["tiempo_en_llamada_segs"] = "";
            $row["tiempo_en_llamada"] = "";
            foreach ($chans as $chan => $currch) {
                if (isset($currch['Context']) && $currch['Context'] == "broadcast" && $currch['Link'] == "Agent/" . $agent && $currch['CallerIDNum'] == $row["numero"]) {
                    $row["tiempo_en_llamada_segs"] = $currch['Seconds'];
                    $row["tiempo_en_llamada"] = sec_to_time($row["tiempo_en_llamada_segs"]);
                }
            }
            $row["fecha"] = $curr['LoggedInTime'];

            $bgcolor = "#FFFFFF";
            if (isset($curr['Status']) && $curr['Status'] != "DESCONECTADO" && isset($curr['Predictivo']) && $curr['Predictivo'] == $req_idp) {
                ?>
                <tr bgcolor="#FFFFFF" height="35" onMouseOver="this.style.backgroundColor = '#CFECFC', this.style.color = '#FFFFFF'" onMouseOut="this.style.backgroundColor = '#FFFFFF', this.style.color = '#000000'";>
                    <th align="center"  ><font size="3" face="Courier New, Courier, monospace" color="#006699"><b>&nbsp;<?php echo $row["anexo"]; ?>&nbsp;</b></font></th>
                    <?php
                    /* $colasdeagente="";
                      $sqlcolasagente="select CC_COLAS.CC_NOMBRE as cola,
                      CC_EXPERIENCIA.CC_DESCRIPCION as experiencia
                      from predictivo.CC_AGENTE,predictivo.CC_COLAS,predictivo.CC_EXPERIENCIA,predictivo.CC_AGENTE_COLAS
                      where CC_AGENTE.CC_ID_AGENTE=CC_AGENTE_COLAS.CC_ID_AGENTE
                      and CC_COLAS.CC_ID_COLAS=CC_AGENTE_COLAS.CC_ID_COLAS
                      and CC_EXPERIENCIA.CC_ID_EXPERIENCIA=CC_AGENTE_COLAS.CC_ID_EXPERIENCIA
                      and CC_AGENTE_COLAS.CC_ESTADO='01'
                      and CC_AGENTE.CC_USUARIO='".$row["agente"]."'";
                      $querycolasagente=mysql_query($sqlcolasagente,$conn);
                      while($rowcolasagente=mysql_fetch_array($querycolasagente))
                      {
                      $colasdeagente.=$rowcolasagente["cola"]."\n";
                      }
                      mysql_free_result($querycolasagente); */
                    ?>
                    <th align="center" title="<?php //echo $colasdeagente;       ?>"><font size="2" face="Courier New, Courier, monospace" color="#006699" ><b>&nbsp;<?php
                            if ($row["agente"] != "") {
                                echo $row["nombre"] . " (" . $row["agente"] . ")";
                            }
                            $bgcolor = "#FFFFFF";
                            switch ($row["estado"]) {
                                case "ONLINE" : $bgcolor = "#00CC33";
                                    break;
                                case "GESTIONANDO" : $bgcolor = "#4599C4";
                                    break;
                                case "LIBRE" : $bgcolor = "#39EE63";
                                    break;
                                case "CONECTADO" : $bgcolor = "#00CC33";
                                    break;
                                case "BACKOFFICE" : $bgcolor = "#00FFFF";
                                    break;
                                case "REFRIGERIO" : $bgcolor = "#FFCC00";
                                    break;
                                case "BREAK" : $bgcolor = "#FFCC00";
                                    break;
                                case "GESTORCORREO" : $bgcolor = "#C4F715";
                                    break;
                                case "GESTORPROVINCIA" : $bgcolor = "#C4F715";
                                    break;
                            }
                            ?>&nbsp;</b></font></th>
                    <th align="center" <?php
                    if ($bgcolor != "#FFFFFF") {
                        echo 'bgcolor="' . $bgcolor . '"';
                    }
                    ?> ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b><?php echo $row["estado"]; ?></b></font></th>
                        <?php
                        $bgcolor = "#FFFFFF";
                        $txcolor = "#000000";


                        if ($row["tipo_llamada"] == 'SALIENTE' && isset($row["tiempo_en_llamada_segs"])) {
                            if ($row["tiempo_en_llamada_segs"] > 0 && $row["tiempo_en_llamada_segs"] <= 300) {
                                $bgcolor = "#F0FF00";
                                $txcolor = "#000000";
                            } elseif ($row["tiempo_en_llamada_segs"] > 300 && $row["tiempo_en_llamada_segs"] <= 600) {
                                $bgcolor = "#FF8600";
                                $txcolor = "#000000";
                            } elseif ($row["tiempo_en_llamada_segs"] > 600) {
                                $bgcolor = "#FF0004";
                                $txcolor = "#FFFFFF";
                            }
                        }
                        ?>
                    <th align="center" <?php
                    if ($bgcolor != "#FFFFFF") {
                        echo 'bgcolor="' . $bgcolor . '"';
                    }
                    ?>><font size="2" <?php echo 'color="' . $txcolor . '"'; ?> face="Verdana, Arial, Helvetica, sans-serif"><b>&nbsp;<?php echo $row["estado_anexo"]; ?>&nbsp;</b></font></th>
                    <th align="center" <?php
                    if ($bgcolor != "#FFFFFF") {
                        echo 'bgcolor="' . $bgcolor . '"';
                    }
                    ?>><font size="3" face="Courier New, Courier, monospace" <?php echo 'color="' . $txcolor . '"'; ?>><b>&nbsp;<?php echo $row["tipo_llamada"]; ?>&nbsp;</b></font></th>
                    <th align="center" <?php
                    if ($bgcolor != "#FFFFFF") {
                        echo 'bgcolor="' . $bgcolor . '"';
                    }
                    ?>><font size="3" face="Courier New, Courier, monospace" <?php echo 'color="' . $txcolor . '"'; ?>><b>&nbsp;<?php echo $row["numero"]; ?>&nbsp;</b></font></th>
                    <th align="center" <?php
                    if ($bgcolor != "#FFFFFF") {
                        echo 'bgcolor="' . $bgcolor . '"';
                    }
                    ?>><font size="3" face="Courier New, Courier, monospace" <?php echo 'color="' . $txcolor . '"'; ?>><b>&nbsp;<?php
                                if (isset($row["tiempo_en_llamada"])) {
                                    echo $row["tiempo_en_llamada"];
                                }//."(".$row["tiempo_en_llamada_segs"]."s)"; 
                                ?>&nbsp;</b></font></th>
                    <th align="center" <?php
                    if ($bgcolor != "#FFFFFF") {
                        echo 'bgcolor="' . $bgcolor . '"';
                    }
                    ?>><font size="3" face="Courier New, Courier, monospace" <?php echo 'color="' . $txcolor . '"'; ?>><b>&nbsp;<?php echo $row["total_llamadas"]; ?>&nbsp;</b></font></th>
                        <?php
                        if ($_SESSION["autentificado_detenlinea"] == "SI") {
                            ?>
                        <th align="center"><?php
                            if ($row["agente"] != "" && $_SESSION["autentificado_detenlinea"] == "SI") {
                                ?><input type="button" name="logout" id="logout" value="LogOut" onClick="javascript:ver_win('btn_logout_agente.php?agente=<?php echo $row["agente"]; ?>&anexo=<?php echo $row["anexo"]; ?>&opc=1', '', 'location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no,titlebar=no,status=no,left=500,top=200,width=300,height=100')"><?php
                            }
                            ?></th>
                        <?php
                    }
                    ?>
                </tr>
                <?php
            }//FIN DEL IF DESCONECTADO
        }
        ?>
    </table>
    <?php
    mysql_close($conn);
}
?>

/*
 * Parametros mandatorios
 */
// seconds el tiempo en que se refresca
// divid el div que quieres actualizar!
// url el archivo que ira en el div

function refreshdivrun(idp) {
    refreshdivcons(idp);
    refreshdivcols(idp);
    //refreshdivfuns();
}

//DETALLE CONSOLIDADO
function refreshdivcons(idp) {

    // The XMLHttpRequest object

    var xmlHttp;
    try {
        xmlHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Tu explorador no soporta AJAX.");
                return false;
            }
        }
    }

    // Timestamp for preventing IE caching the GET request
    var timestamp = parseInt(new Date().getTime().toString().substring(0, 10));
    var nocacheurl = "detconsol.php" + "?t=" + timestamp + "&idp=" + idp;

    // The code...

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.readyState != null) {
            document.getElementById('detconsol').innerHTML = xmlHttp.responseText;
            setTimeout('refreshdivcons()', 2000);
        }
    }
    xmlHttp.open("GET", nocacheurl, true);
    xmlHttp.send(null);
}

//DETALLE DE COLAS
function refreshdivcols(idp) {

    // The XMLHttpRequest object

    var xmlHttp;
    try {
        xmlHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Tu explorador no soporta AJAX.");
                return false;
            }
        }
    }

    // Timestamp for preventing IE caching the GET request
    var timestamp = parseInt(new Date().getTime().toString().substring(0, 10));
    var nocacheurl = "detenlinea.php" + "?t=" + timestamp + "&idp=" + idp;

    // The code...

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.readyState != null) {
            document.getElementById('detenlinea').innerHTML = xmlHttp.responseText;
            setTimeout('refreshdivcols()', 2000);
        }
    }
    xmlHttp.open("GET", nocacheurl, true);
    xmlHttp.send(null);
}

//FUNCIONES GENERA
function refreshdivfuns(idp) {

    // The XMLHttpRequest object

    var xmlHttp;
    try {
        xmlHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Tu explorador no soporta AJAX.");
                return false;
            }
        }
    }

    // Timestamp for preventing IE caching the GET request
    var timestamp = parseInt(new Date().getTime().toString().substring(0, 10));
    var nocacheurl = "detfunciones.php" + "?t=" + timestamp + "&idp=" + idp;

    // The code...

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.readyState != null) {
            document.getElementById('detfunciones').innerHTML = xmlHttp.responseText;
            setTimeout('refreshdivfuns()', 5000);
        }
    }
    xmlHttp.open("GET", nocacheurl, true);
    xmlHttp.send(null);
}

//AGENTE INICIO ESTADOS
function refreshdivagnt(agente, anexo, estado) {

    // The XMLHttpRequest object

    var xmlHttp;
    try {
        xmlHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Tu explorador no soporta AJAX.");
                return false;
            }
        }
    }

    // Timestamp for preventing IE caching the GET request
    var timestamp = parseInt(new Date().getTime().toString().substring(0, 10));
    var nocacheurl = "agente_estados.php" + "?t=" + timestamp + "&agente=" + agente + "&anexo=" + anexo + "&estado=" + estado;

    // The code...

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.readyState != null) {
            document.getElementById('agentestados').innerHTML = xmlHttp.responseText;
            setTimeout('refreshdivagnt()', 5000);
        }
    }
    xmlHttp.open("GET", nocacheurl, true);
    xmlHttp.send(null);
}

// Empieza la función de refrescar

/*window.onload = function(){
 refreshdiv2(); // corremos inmediatamente la funcion
 }*/
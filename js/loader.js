jQuery.fn.Loader = function (action) {
	if(action=='destroy'){
		$(".jquery_loader_ico", $(this)).fadeOut('fast');		
	}else{
		// Destruimos el fondo de cargando si es que habia uno anterior
		$(".jquery_loader_ico", $(this)).remove();
		// Agregamos la clase
		$(this).addClass('jquery_loader');
		// Mostramos el fondo de cargando
		$(this).prepend('<div class="jquery_loader_ico">');		
	}
}
